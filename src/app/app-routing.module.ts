import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../@vex/layout/layout.component';
const routes: Routes = [
  // {
  //   path: '',
  //   component: LayoutComponent,
  //   children: []
  // },
  { path: '',
  loadChildren: () => import('src/@vex/components/college-index/college-index.module').then(m => m.CollegeIndexModule) 
},
{ path: 'signup',
loadChildren: () => import('src/@vex/components/sign-up/sign-up.module').then(m => m.SignUpModule) 
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
