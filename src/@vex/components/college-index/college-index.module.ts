import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollegeIndexComponent } from './college-index.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CollegeIndexComponent,
    children: []
  }
];

@NgModule({
  declarations: [CollegeIndexComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CollegeIndexModule { }
