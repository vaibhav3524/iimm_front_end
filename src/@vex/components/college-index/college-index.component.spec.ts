import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeIndexComponent } from './college-index.component';

describe('CollegeIndexComponent', () => {
  let component: CollegeIndexComponent;
  let fixture: ComponentFixture<CollegeIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollegeIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
